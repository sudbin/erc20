<h1 align="center">This is an example of an <a href="https://docs.openzeppelin.com/contracts/4.x/erc20" target="_blank">ERC 20 
standart</a></h1>

[![Typing SVG](https://readme-typing-svg.herokuapp.com?font=Fira+Code&duration=2000&pause=1000&lines=Fixed+Supply;Rewarding+Miners;Access+level;Hardhat;Ethers;Binance+smart+chain)](https://git.io/typing-svg)

## Getting Started

1. В данном репозитории рассматривается полный цикл взаимодействия со стандартом erc20
    - [Fixed Supply](./README.md#fixed-supply)
    - [Rewarding Miners](./README.md#rewarding-miners)
    - [Access level](./docs/access_control.md)
2. Документация [hardhat](https://hardhat.org/hardhat-runner/docs/config)

## Installation

1. `yarn`
2. `cp .env.example .env` - далее необходимо настроить свои ключи
3. `npx hardhat compile` -> `hh compile` need install globally `hardhat-shorthand`
    - Команда установки: `yarn global add hardhat-shorthand`.
    - Теперь можно использовать синтаксис: `hh {commandName}`

## Debug

1. `hh compile` - соберет смарт-контракт в папку `/artifacts`
2. Деплой и тестирование
    - Запустить тестовую ноду локально: `hh node`
    - Опубликовать смарт-контракты: `hh deploy` - код для публикации должен быть в папке deploy
3. Возможные ошибки:
    - Обязательно проверить все переменные `.env`, особенно `WALLET_PRIVATE_KEY` и `NODE_ENV`
    - Если подвисает нода: поменять rpc url - `https://chainlist.org/`

Result example:

```
Starting...
RPC URL: https://data-seed-prebsc-1-s3.binance.org:8545
deploying "MyToken" (tx: 0xf5dd4f69a0ea0ec929be2019e7c51bbc6c4b2fb6fe8845cc538125dda71104cd)...: deployed at 0x4875EA4b0D1d739588831839c3Dfea097AD2C9a0 with 828790 gas
MyToken at 0x4875EA4b0D1d739588831839c3Dfea097AD2C9a0
Start verify...
Verifying contract...
Nothing to compile
No need to generate any newer typings.
Successfully submitted source code for contract
contracts/MyToken.sol:MyToken at 0x4875EA4b0D1d739588831839c3Dfea097AD2C9a0
for verification on the block explorer. Waiting for verification result...

Successfully verified contract MyToken on Etherscan.
https://testnet.bscscan.com/address/0x4875EA4b0D1d739588831839c3Dfea097AD2C9a0#code
Verification finish
```

### Fixed Supply

```
constructor() ERC20("Fixed", "FIX") {
    _mint(msg.sender, 1000); // Использовать для отдельного минта или в момент создания контракта (как в этом примере)
}
```

### Rewarding Miners

`block.coinbase` - адрес майнера которому можно выдавать вознаграждение

Internal management:

```
function mintMinerReward() public {
    _mint(block.coinbase, 1000);
}
```

External management (`ERC20PresetMinterPauser`)

```
function _beforeTokenTransfer(address from, address to, uint256 value) internal virtual override {
    if (!(from == address(0) && to == block.coinbase)) {
        _mintMinerReward();
    }
    super._beforeTokenTransfer(from, to, value);
}
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
