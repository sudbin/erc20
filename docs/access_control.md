# Access Level

1. По умолчанию контракт — owner это Ownable учетная запись, которая его развернула.

```
contract MyContract is Ownable {
    function normalThing() public {
        // anyone can call this normalThing()
    }

    function specialThing() public onlyOwner {
        // only the owner can call specialThing()!
    }
}
```

2. Ownable также позволяет:
    -   `transferOwnership` с учетной записи владельца на новую
    -   `renounceOwnership` для владельца отказаться от этой административной привилегии, общий шаблон после завершения начального этапа с централизованным администрированием.
    -   **Контракт** также может быть владельцем другого контракта

#### Пример работы с ролями

```
contract MyToken is ERC20, AccessControl {
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant BURNER_ROLE = keccak256("BURNER_ROLE");

    constructor(address minter, address burner) ERC20("MyToken", "TKN") {
        _setupRole(MINTER_ROLE, minter);
        _setupRole(BURNER_ROLE, burner);
    }

    function mint(address to, uint256 amount) public onlyRole(MINTER_ROLE) {
        _mint(to, amount);
    }

    function burn(address from, uint256 amount) public onlyRole(BURNER_ROLE) {
        _burn(from, amount);
    }
}
```

3. Также есть DEFAULT_ADMIN_ROLE - ее можно использовать без объявления
4. Для ограничения злоуптреблением полномочий владельца смарт-контракта можно отложить выполнение каждой операции для проверки ее пользователями и соответствующей реакции